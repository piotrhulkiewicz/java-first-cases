package Exeptions.Exercises.CompetitionApp;

public class AgeViolationException extends RuntimeException {
    private int requiredAge;

    public AgeViolationException(int requiredAge) {
        this.requiredAge = requiredAge;
    }

    public int getRequiredAge() {
        return requiredAge;
    }
}
