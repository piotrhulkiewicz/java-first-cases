package Exeptions.Exercises.CompetitionApp;

public class MaxCompetitorsException extends RuntimeException{
    private int maxCompetitors;

    public MaxCompetitorsException (int maxCompetitors) {
        this.maxCompetitors = maxCompetitors;
    }

    public int getMaxCompetitors() {
        return maxCompetitors;
    }
}
