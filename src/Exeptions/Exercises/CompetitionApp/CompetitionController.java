package Exeptions.Exercises.CompetitionApp;

import java.util.InputMismatchException;
import java.util.Scanner;

class CompetitionController {
    private Scanner scanner = new Scanner(System.in);

    void run() {
        Competition competition = createCompetition();
        fillParticipantsInfo(competition);
        printCompetition(competition);
    }

    private Competition createCompetition() {
        System.out.println("Podaj nazwę zawodów:");
        String competitionName = scanner.nextLine();
        System.out.println("Podaj maksymalną liczbę uczestników:");
        int maxParticipants = readPositiveNumber();
        System.out.println("Podaj ograniczenie wiekowe:");
        int ageLimit = readPositiveNumber();
        return new Competition(competitionName, maxParticipants, ageLimit);
    }

    private void fillParticipantsInfo(Competition competition) {
        while (competition.hasFreeSpots()) {
            System.out.println("Dodaj nowego uczestnika");
            Participant participant = createParticipant();
            try {
            competition.addParticipant(participant);
            } catch (AgeViolationException e) {
                System.err.println("Not possible to add new participant because of low age. Age required: "
                        + e.getRequiredAge());
            } catch (MaxCompetitorsException e) {
                System.err.println("Not possible to add new participant because max limit is reached. Limit is: "
                + e.getMaxCompetitors());
            } catch (DuplicateException e) {
                System.err.println("The participant with id: " + e.getParticipant() + " is already on the list");
            }
        }

    }

    private Participant createParticipant() {
        System.out.println("Podaj imię:");
        String firstName = scanner.nextLine();
        System.out.println("Podaj nazwisko:");
        String lastName = scanner.nextLine();
        System.out.println("Podaj id (np. pesel):");
        String id = scanner.nextLine();
        System.out.println("Podaj wiek:");
        int age = readPositiveNumber();
        return new Participant(firstName, lastName, id, age);
    }

    private void printCompetition(Competition competition) {
        System.out.println(competition.toString());
    }

    private int readPositiveNumber() {
        int number = -1;
        while (number <= 0) {
            try {
                number = scanner.nextInt();
                if (number <= 0) {
                    System.err.println("Enter positive number. Try again");
                }
            } catch (InputMismatchException e) {
                System.err.println("Enter positive number. Try again");
            } finally {
                scanner.nextLine();
            }
        }
        return number;
    }
}