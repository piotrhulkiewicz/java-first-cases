package Exeptions.Exercises.CalculatorApp;

public class UnknownOperatorException extends Exception {
    public UnknownOperatorException(String message) {
        super(message);
    }
}
