package Exeptions.Exercises.CalculatorApp;

public class Calculate {

    public double calculate(String option, double a, double b) throws UnknownOperatorException {
        double result = 0;
        switch (option) {
            case "+" -> result = addTwoNumbers(a, b);
            case "-" -> result = subtractTwoNumbers(a, b);
            case "*" -> result = multiplyTwoNumbers(a, b);
            case "/" -> result = divideTwoNumbers(a, b);
            default -> throw new UnknownOperatorException("invalid operator");
        }
        return result;
    }

    private double addTwoNumbers(double a, double b) {
        return a + b;
    }

    private double subtractTwoNumbers(double a, double b) {
        return a - b;
    }

    private double multiplyTwoNumbers(double a, double b) {
        return a * b;
    }

    private double divideTwoNumbers(double a, double b) {
        if (b != 0) {
            return a / b;

        } else throw new ArithmeticException("You can't divide by 0!");
    }
}
