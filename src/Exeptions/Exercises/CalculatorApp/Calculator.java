package Exeptions.Exercises.CalculatorApp;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        try {
            System.out.println("Enter a ");
            double a = scanner.nextDouble();

            System.out.println("Enter b ");
            double b = scanner.nextDouble();
            scanner.nextLine();
            System.out.println("""
                    Enter operator: 
                    '+' - To add numbers
                    '-' - To subtract numbers
                    '*' - To multiply numbers
                    '/' - To divide numbers
                    """);
            String operator = scanner.nextLine();

            Calculate calculate = new Calculate();
            try {
                System.out.println(a + " " + operator + " " + b + " = " + calculate.calculate(operator, a, b));
            } catch (UnknownOperatorException | ArithmeticException exception) {
                System.err.println(exception.getMessage());
            }
        } catch (InputMismatchException e) {
            System.err.println("Enter number!");
        }
    }
}