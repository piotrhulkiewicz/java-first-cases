package Exeptions.DefineOwnExceptions;

public class School {

    private int MAX_STUDENTS;
    private int numberOfStudents = 0;
    private Student[] students;

    public School (int MAX_STUDENTS) {
        this.students = new Student[MAX_STUDENTS];
    }

    public void addStudent (Student student ) {
        if (numberOfStudents < students.length) {
            students[numberOfStudents] = student;
            numberOfStudents++;
        } else throw new ArrayIndexOutOfBoundsException("No place for another student");
    }

    public Student compare (String firstName, String lastName) throws NoMatchException {
        for (Student student : students) {
            if (student.getFirstName().equals(firstName) && student.getLastName().equals(lastName)) {
                return student;
            }
        }  throw new NoMatchException("There is no student " + firstName + " " + lastName + " in school" );
    }

    public void printInfo () {
        System.out.println("List of students: ");
        for (Student student : students) {
            System.out.println(student);
        }

    }

    public void printCompared (Student student) {
        System.out.println("Selected student is: \n" + student);
    }
}
