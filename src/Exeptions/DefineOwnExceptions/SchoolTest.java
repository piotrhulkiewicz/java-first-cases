package Exeptions.DefineOwnExceptions;

public class SchoolTest {

    public static void main(String[] args) {

        School school = new School(3);
        Student student1 = new Student(1, "John", "Rambo");
        Student student2 = new Student(2, "Bilbo", "Baggins");
        Student student3 = new Student(3, "Gandalf", "Grey");
        Student student4 = new Student(4, "Peregrine", "Tuk");

        try {
            school.addStudent(student1);
            school.addStudent(student2);
            school.addStudent(student3);
            school.addStudent(student4);
        } catch (ArrayIndexOutOfBoundsException exception) {
            System.err.println(exception.getMessage());
        }
        school.printInfo();
        try {
            Student studentToFind = school.compare("John", "Rambo");
            school.printCompared(studentToFind);
        } catch (NoMatchException e) {
            System.err.println(e.getMessage());
        }

    }
}
