package Exeptions.TryCatch;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TryCatch {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int [] numbers = new int[2];
        boolean error = true;
        do {
            try {
                System.out.println("Enter first number");
                numbers[0] = scanner.nextInt();
                System.out.println("Enter second number");
                numbers[1] = scanner.nextInt();
                System.out.println("Which number to print? 1 or 2");
                System.out.println("Number is " + numbers[scanner.nextInt()-1]);
                error = false;
            } catch (InputMismatchException exception) {
                System.err.println("Only natural numbers are correct. Try again");
            } catch (IndexOutOfBoundsException exception) {
                System.err.println("choose only 1 or 2");
            } finally {
                scanner.nextLine();
            }
        } while (error);
        scanner.close();
    }
}
