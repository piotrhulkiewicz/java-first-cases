package Exeptions.Throw;

public class Car {

    private static final double MAX_FUEL = 50;
    private static final double FUEL_CONSUMPTION_FOR_100KM = 8;
    double fuel;

    public void addFuel (double addedFuel) {
        if (fuel + addedFuel > MAX_FUEL)
            throw new IllegalArgumentException("You can't refuel your car. Maximum amount of fuel to add is: " + (MAX_FUEL - fuel));
        else fuel += addedFuel;
    }

    public void drive() {
        if (fuel < FUEL_CONSUMPTION_FOR_100KM)
            throw new IllegalStateException("You can't drive 100 kilometres. Refuel!");
         else fuel -= FUEL_CONSUMPTION_FOR_100KM;
    }

    @Override
    public String toString() {
        return "Amount of fuel: " + fuel;
    }
}
