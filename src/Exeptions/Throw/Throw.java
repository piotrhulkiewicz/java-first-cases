package Exeptions.Throw;

public class Throw {

    public static void main(String[] args) {

        Car car = new Car();
        try {
            car.addFuel(70);
            System.out.println(car);
            car.drive();
            System.out.println(car);
            car.drive();
            System.out.println(car);
            car.drive();
            System.out.println(car);
        } catch (IllegalStateException | IllegalArgumentException e) {
            System.err.println(e.getMessage());
        }
    }
}
