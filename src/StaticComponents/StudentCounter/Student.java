package StaticComponents.StudentCounter;

class Student {

    private String firstName;
    private String lastName;
    private String studentID;

    private static int NUMBER_OF_STUDENTS;

    public Student (String firstName, String lastName, String studentID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.studentID = studentID;
        NUMBER_OF_STUDENTS++;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public static int getNumberOfStudents() {
        return NUMBER_OF_STUDENTS;
    }

    public String studentInfo () {
        return getFirstName() + " " + getLastName() + "ID: " + getStudentID();
    }
}
