package StaticComponents.StudentCounter;

public class University {
    public static void main(String[] args) {


        Student student1 = new Student("John", "Rambo", "11001");
        Student student2 = new Student("Merry", "Pippin", "11002");

        System.out.println("Number od students at the university is: " + Student.getNumberOfStudents());
        System.out.println(student1.studentInfo());
        System.out.println(student2.studentInfo());
    }
}
