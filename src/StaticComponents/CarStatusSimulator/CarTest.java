package StaticComponents.CarStatusSimulator;

public class CarTest {
    public static void main(String[] args) {

        Car car = new Car(10, true, true, true);
        car.start();
        System.out.println(car.status());
        car.outOfFuel();
        System.out.println(car.status());
        System.out.println(car.start());
        car.fillFuel(20);
        car.doorOpen();
        car.start();
        car.start();

//        car.stopEngine();


    }
}
