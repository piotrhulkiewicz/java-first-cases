package StaticComponents.CarStatusSimulator;

public class Car {

    private double fuelLevel;
    private boolean engineOK;
    private boolean fillerFlapClosed;
    private boolean doorsLocked;
    private boolean engineIsStarted;

    private final static int ENGINE_STARTED = 100;
    private final static int ENGINE_NOT_STARTED_ENGINE_FAILURE = 200;
    private final static int ENGINE_NOT_STARTED_OUT_OF_FUEL = 300;
    private final static int ENGINE_STARTED_FILLER_FLAP_OPENED = 400;
    private final static int ENGINE_STARTED_DOOR_OR_TRUNK_OPENED = 500;

    public Car(double fuelLevel, boolean engineOK, boolean fillerFlapClosed, boolean doorsLocked) {
        this.fuelLevel = fuelLevel;
        this.engineOK = engineOK;
        this.fillerFlapClosed = fillerFlapClosed;
        this.doorsLocked = doorsLocked;

    }

    public double getFuelLevel() {
        return fuelLevel;
    }

    public boolean isEngineOK() {
        return engineOK;
    }

    public boolean isFillerFlapClosed() {
        return fillerFlapClosed;
    }

    public boolean isDoorsLocked() {
        return doorsLocked;
    }

    public boolean isEngineIsStarted() {
        return engineIsStarted;
    }

    public void fillFuel(int fuel) {
        fuelLevel = getFuelLevel() + fuel;
    }
    public void setFuelLevel(double fuelLevel) {
        if (fuelLevel <0 ) System.out.println("Fuel level can not be negative");
        else {
        this.fuelLevel = fuelLevel;}
    }

    public void outOfFuel() {
        fuelLevel = 0;
    }

    public void openFillerFlap() {
        fillerFlapClosed = false;
    }

    public void doorOpen() {
        doorsLocked = false;
    }

    public void engineFailure() {
        engineOK = false;
    }

    public void stopEngine() {
        engineIsStarted = false;
    }

    public int start() {
        int message = 0;
        if (engineIsStarted) {
            System.out.println("engine is already started");
        } else {
            if (fuelLevel > 0 && engineOK && doorsLocked && fillerFlapClosed) {
                System.out.println("All systems works, you can start your journey");
                engineIsStarted = true;
                message = ENGINE_STARTED;
            }
            if (!engineOK) {
                System.out.println("Check engine");
                engineIsStarted = false;
                message = ENGINE_NOT_STARTED_ENGINE_FAILURE;
            }
            if (fuelLevel == 0) {
                System.out.println("Out of fuel");
                engineIsStarted = false;
                message = ENGINE_NOT_STARTED_OUT_OF_FUEL;
            }
            if (fuelLevel > 0 && !fillerFlapClosed) {
                engineIsStarted = true;
                System.out.println("Filler flap is open");
                message = ENGINE_STARTED_FILLER_FLAP_OPENED;
            }
            if (fuelLevel > 0 && !doorsLocked) {
                engineIsStarted = true;
                System.out.println("Door or trunk is opened");
                message = ENGINE_STARTED_DOOR_OR_TRUNK_OPENED;
            }
        }
        return message;
    }

    public String status() {
        String status = null;

        if (fuelLevel == 0) {
            status = "Out of fuel";
            stopEngine();
        } else if (engineOK && doorsLocked && fillerFlapClosed && engineIsStarted) {
            status = "All systems works";
        }
        if (!engineOK) {
            status = "Check engine";
        }
        if (!doorsLocked) {
            status = "Door is open";
        }
        if (!fillerFlapClosed) {
            status = "Filler flap is open";
        }
        return status;
    }
}
