package StaticComponents.KitchenConverter;


public class KitchenConverter {

    public static void main(String[] args) {
        double glassOFWater = 2.5;
        double spoonOFOil = 1.5;
        double smallSpoonOfLemonJuice = 0.5;


        System.out.println("Add: 2,5 glasses of water, 3,5 spoons of oil and 0,5 little spoon of lemon juice");
        System.out.println("2,5 glasses of water is " + KitchenUtils.CONVERT_GLASS(glassOFWater) + " millilitres");
        System.out.println("3,5 spoons of oil is " + KitchenUtils.CONVERT_SPOON(spoonOFOil) + " millilitres");
        System.out.println("0,5 small spoons of lemon juice is " + KitchenUtils.CONVERT_SMALL_SPOON(smallSpoonOfLemonJuice) + " millilitres");
    }

}
