package StaticComponents.KitchenConverter;

public class KitchenUtils {

    static double GLASS_CAPACITY_IN_MILLIMITERS = 250;
    static double SPOON_CAPACITY_IN_MILLIMITERS = 15;
    static double SMALL_SPOON_CAPACITY_IN_MILLIMITERS = 5;

    static double CONVERT_GLASS (double glass) {
        return glass * GLASS_CAPACITY_IN_MILLIMITERS;
    }

    static double CONVERT_SPOON (double spoon) {
        return spoon * SPOON_CAPACITY_IN_MILLIMITERS;
    }

    static double CONVERT_SMALL_SPOON (double littleSpoon) {
        return littleSpoon * SMALL_SPOON_CAPACITY_IN_MILLIMITERS;
    }
}
