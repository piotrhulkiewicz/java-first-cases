package MethodsAndConstructorsOverloading.CalculatorApp;

public class Calculator {

    double add ( double a, double b){
        return a + b;
    }

    double add ( double a, double b, double c){
        return a+b+c;
    }

    double add (int a, double b) {
        return a+b;
    }
}
