package MethodsAndConstructorsOverloading.CalculatorApp;

public class CalcTest {
    public static void main(String[] args) {
        Calculator calc = new Calculator();
        double sum = calc.add(4, 5.5);
        System.out.println(sum);

        double sum2 = calc.add(2.3, 6.0, 4.5);

        double sum3 = calc.add(sum2, 1.1);
        System.out.println(sum3);
    }
}
