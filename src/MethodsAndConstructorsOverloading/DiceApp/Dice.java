package MethodsAndConstructorsOverloading.DiceApp;

import java.util.Random;

public class Dice {

    int bottom;
    Dice () {
        roll();
    }
    Dice (int bottom) {
        this.bottom = bottom;
    }

    void roll () {
        Random rand = new Random();
        bottom = rand.nextInt(1, 7);
    }

    void rollInfo () {
        roll();
        System.out.println("Liczba oczek na kostce: " + bottom);
    }
}
