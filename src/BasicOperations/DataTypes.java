package BasicOperations;

public class DataTypes {
    public static void main(String[] args) {
        System.out.println("Hi Piotr, i heard, you are " + 43 + " and you study programming in Java. Is it true?\n");
        System.out.println(true);
        System.out.println("Are your initials " + 'P' + 'H'+ '?');
        System.out.println("Yes," + true);
    }
}

class DataTypesExercise {
    public static void main(String[] args) {
        System.out.println("Mam Audi A4");
        System.out.println("Rocznik:");
        System.out.println(2004);
        System.out.println("Silnik o pojemności:");
        System.out.println(2.0);
    }
}
