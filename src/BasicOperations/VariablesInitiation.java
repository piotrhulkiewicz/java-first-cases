package BasicOperations;

public class VariablesInitiation {
    public static void main(String[] args) {
        String brand = "Ford";
        String model = "Fiesta";
        int productionYear = 2020;
        double price = 65_000;
        String color = "red";

        System.out.println("I want to sell ClassesAndObjects.a car: " + brand + ", model: " + model + ", year of production: " +
                productionYear + ", color: " + color + ", price: " + price + '.' );
    }
}

class Stats {
    public static void main(String[] args) {
        final String url = "https://javastart.pl";

        System.out.print("Strona, z której uczę się Javy to: ");
        System.out.println(url);

        int kevinYear = 1990;
        System.out.print("Film Kevin sam w domu został wydany w roku: ");
        System.out.println(kevinYear);

        var voterTurnout = 58.0;
        voterTurnout = 61.1;
        System.out.print("Procentowa frekwencja podczas ostatnich wyborów wyniosła: ");
        System.out.println(voterTurnout);
    }
}

