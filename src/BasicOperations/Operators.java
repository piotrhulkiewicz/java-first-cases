package BasicOperations;

import java.util.Random;

class MathOperators {
    public static void main(String[] args) {
        int x = 10;
        int y = 4;
        int sum = x + y; // dodawanie
        int sub = x - y; // odejmowanie
        int multi = x * y; // mnożenie
        double div = x / y; // dzielenie całkowitoliczbowe
        int mod = x % y;

        System.out.println(sum);
        System.out.println(sub);
        System.out.println(multi);
        System.out.println(div);
        System.out.println(mod);
    }
}

class MathOperators2 {
    public static void main(String[] args) {
        double x = 10;
        int y = 4;
        double div = x / y; // dzielenie

        System.out.println(div);
    }
}

class ShorthandOperators {
    public static void main(String[] args) {
        int x = 10;
        x += 5;
        System.out.println(x); // x ma wartość 15
        x *= 3;
        System.out.println(x); // x ma wartość 45
        x /= 5;
        System.out.println(x); // x ma wartość 9
        x -= 2;
        System.out.println(x); // x ma wartość 7
        x %= 4;
        System.out.println(x);// x ma wartość 3
    }
}

class CompareOperators {
    public static void main(String[] args) {
        int x = 5;
        int y = 10;
        System.out.print("Czy X jest większy od Y? ");
        System.out.println(x > y);

        System.out.print("Czy X jest różny od Y? ");
        System.out.println(x != y);

        boolean instance = "Kasia" instanceof java.lang.String;
        System.out.println(instance); // "Kasia" jest typu String
    }
}

class LogicalOperators {
    public static void main(String[] args) {
        int x = 5;
        int y = 10;
        boolean logic = true;

        System.out.print("x<10 && y<11? ");
        System.out.println(x < 10 && y < 11);

        System.out.print("x<10 && y<10? ");
        System.out.println(x < 10 && y < 10);

        System.out.print("x<10 || y<10? ");
        System.out.println(x < 10 || y < 10);

        System.out.print(logic);
        System.out.print(" , !logic = ");
        System.out.println(!logic);

        System.out.print("Warunek złożony !(x<10 && y<10) ");
        System.out.println(!(x < 10 && y < 10));
    }
}

class UpAndDown {
    public static void main(String[] args) {
        int num = 3;
        // najpierw wyświetla, później zwiększa
        System.out.println(num++);
        System.out.println(num);
        // najpierw zwiększa, później wyświetla
        System.out.println(++num);
        System.out.println(num);
    }
}

class Concat {
    public static void main(String[] args) {
        String s1 = "Hello";
        String s2 = " World!";
        String s3 = s1 + s2;

        System.out.println(s3);

        // konkatenacji możemy używać bezpośrednio w print()
        System.out.println("Uczę się Javy od " + 2 + " dni");
        // można też łączyć inne typy
        System.out.println("Kasia is beautiful. It is " + true);
    }
}

class Conversions {
    public static void main(String[] args) {
        double number1 = 10.987;
        int number2 = 5;

        // konwersja zawężająca (eng. narrowing)
        int narrowing = (int) number1;

        // konwersja rozszerzająca (eng. widening)
        double widening = (double) number2;

        System.out.println("Narrowing: " + narrowing);
        System.out.println("Widening: " + widening);
    }
}

class Conversions2 {
    public static void main(String[] args) {
        char letter1 = 'k';
        char letter2 = 'o';
        char letter3 = 't';
        System.out.println("Błędne \"sumowanie\" znaków");
        System.out.println(letter1 + letter2 + letter3);
        System.out.println("Tekst jawny");
        System.out.println("" + letter1 + letter2 + letter3);
        letter1++;
        letter2++;
        letter3++;
        System.out.println("Tekst zaszyfrowany");
        System.out.println("" + letter1 + letter2 + letter3);
    }
}

class OperatorsExercise1 {
    public static void main(String[] args) {

        Random rand = new Random();
        int x = rand.nextInt(10);
        int y = rand.nextInt(10);
        boolean result;
        System.out.println("x = " + x);
        System.out.println("y = " + y);
        result = x>y;
        System.out.println("x>y? " + result);
        result = x*2 > y;
        System.out.println("x*2 > y? " +result);
        result = y < x+3 && y > x-2;
        System.out.println("y < x+3 && y > x-2? " + result);
        result = (x*y)%2 == 0;
        System.out.println("is x*y even? " + result);


    }
}

class OperatorsExercise2 {
    public static void main(String[] args) {
        double netPrice = 100;
        double taxRate = 0.23;
        double tax = netPrice * taxRate;
        double grossPrice = netPrice + tax;

        System.out.println("Net price = " + netPrice + ", tax =" + tax + ", Gross price = " + grossPrice);


    }
}


