package EnumerationFile.Basic;

import java.util.Scanner;

public class ShirtShop {

    public static void main(String[] args) {


        System.out.println("Select size of a T-shirt");
//        System.out.println(Size.XSMALL.ordinal() + " " + Size.XSMALL.getDescription() + " - " + Size.XSMALL.name());
//        System.out.println(Size.SMALL.ordinal() + " " + Size.SMALL.getDescription() + " - " + Size.SMALL.name());
//        System.out.println(Size.MEDIUM.ordinal() + " " + Size.MEDIUM.getDescription() + " - " + Size.MEDIUM.name());
//        System.out.println(Size.LARGE.ordinal() + " " + Size.LARGE.getDescription() + " - " + Size.LARGE.name());

        Size[] values = Size.values();
        for (Size value : values) {
            System.out.println(value.toString());
        }
        Scanner scanner = new Scanner(System.in);

        int size = scanner.nextInt();
        Size shirtsize = Size.fromOrdinal(size);

        Tshirt tshirt = new Tshirt();
        tshirt.setSize(shirtsize);

        System.out.println("created T-shirt in size: " + tshirt.getSize() + " - " +tshirt.getSize().getDescription());
        scanner.close();
    }
}
