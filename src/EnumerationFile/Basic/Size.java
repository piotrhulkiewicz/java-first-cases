package EnumerationFile.Basic;

public enum Size {
    XSMALL("bardzo mały"), SMALL("mały"), MEDIUM("średni"), LARGE("duży");
    // public static final Size SMALL = new Size("mały")
    private final String description;

    Size(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
    public static Size fromOrdinal (int ordinal) {
        Size[] values = values();
        for (Size size : values) {
            if (size.ordinal() == ordinal)
                return size;
        } return null;
    }

    @Override
    public String toString() {
        return ordinal() + " -  " + getDescription() + " " + name();
    }

}
