package EnumerationFile.DeliverySystem;

public class OrderData {
    private final static int maxOrders = 100;
    private Order[] orders = new Order[maxOrders];
    private int currentNumberOfOrders;


    public void addOrder(Order order) {
        if (currentNumberOfOrders < orders.length) {
            orders[currentNumberOfOrders] = order;
            currentNumberOfOrders++;
        } else System.out.println("Max limit of orders reached");
    }

    public int countStatus(OrderStatus orderStatus) {
        int counter = 0;
        for (int i = 0; i < currentNumberOfOrders; i++) {
            if (orders[i].getStatus() == orderStatus)
                counter++;
        }
        return counter;
    }

    public Order[] selectOrdersByStatus(OrderStatus orderStatus, int counter) {
        Order[] selectedOrders = new Order[counter];
        int count = 0;
        for (int i = 0; i < currentNumberOfOrders; i++) {{
                if (orders[i].getStatus() == orderStatus) {
                    selectedOrders[count] = orders[i];
                    count++;
                }
            }
        }
        return selectedOrders;
    }

    public void printOrders() {
        for (Order order : orders) {
            if (order != null) {
                System.out.println(order);
            }
        }
    }

    public void printNewOrders(Order[] orders) {
        for (Order order : orders) {
                System.out.println(order.getStatus().toString());
            }
        }
    }
