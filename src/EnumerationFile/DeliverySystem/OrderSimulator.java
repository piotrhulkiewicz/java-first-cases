package EnumerationFile.DeliverySystem;

import java.util.Scanner;

public class OrderSimulator {

    public static void main(String[] args) {

        Order order1 = new Order("Notebook Asus", 2999, OrderStatus.NEW);
        Order order2 = new Order("Toyota Aigo", 56479, OrderStatus.PAID);
        Order order3 = new Order("Panasonic TV", 3999, OrderStatus.PAID);
        Order order4 = new Order("Lamp", 299, OrderStatus.SHIPPED);
        Order order5 = new Order("Mouse", 99, OrderStatus.DELIVERED);
        Order order6 = new Order("Chair", 499, OrderStatus.CANCELLED);

        OrderData orderData = new OrderData();
        orderData.addOrder(order1);
        orderData.addOrder(order2);
        orderData.addOrder(order3);
        orderData.addOrder(order4);
        orderData.addOrder(order5);
        orderData.addOrder(order6);

        System.out.println("Odrers in the system: ");
        orderData.printOrders();

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter order status to select orders");
        String fromUser = scanner.nextLine();

        OrderStatus userChoice = OrderStatus.fromName(fromUser);

        int numberOfFilteredOrdersByStatus = orderData.countStatus(userChoice);
        Order[] filteredOrders = orderData.selectOrdersByStatus(userChoice, numberOfFilteredOrdersByStatus);
        orderData.printNewOrders(filteredOrders);
    }
}