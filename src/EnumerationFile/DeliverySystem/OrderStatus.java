package EnumerationFile.DeliverySystem;

public enum OrderStatus {
    NEW("Nowe"),
    PAID("Opłacone"),
    SHIPPED("Wysłane"),
    DELIVERED("Dostarczone"),
    CANCELLED("Anulowane");

    private final String description;

    OrderStatus(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public static OrderStatus fromName (String selectedName) {
        OrderStatus[] values = values();
        for (OrderStatus orderStatus : values) {
            if (orderStatus.name().equals(selectedName)) {
                return orderStatus;
            }
        } return null;
    }

    @Override
    public String toString() {
        return description;
    }
}
