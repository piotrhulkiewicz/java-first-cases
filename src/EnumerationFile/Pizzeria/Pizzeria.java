package EnumerationFile.Pizzeria;

import EnumerationFile.Basic.Size;
import EnumerationFile.Basic.Tshirt;

import java.util.Scanner;

public class Pizzeria {

    public static void main(String[] args) {

        System.out.println("Select pizza to order");
        Pizza[] pizzas = Pizza.values();
        for (Pizza pizza : pizzas) {
            System.out.println(pizza.toString());
        }
        Scanner scanner = new Scanner(System.in);


        int selectedPizza = scanner.nextInt();
        Pizza pizza = Pizza.fromOrdinal(selectedPizza);

        System.out.println("Ordered pizza is: " + pizza.name());
        scanner.close();
    }
}
