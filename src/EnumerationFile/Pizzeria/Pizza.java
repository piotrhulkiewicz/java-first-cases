package EnumerationFile.Pizzeria;

import EnumerationFile.Basic.Size;

public enum Pizza {
    MARGHERITA(true, false, true, false),
    CAPRICIOSA(true, false, true, true),
    PROSCIUTTO(true, true, true, false);

    private final boolean tomatoSauce;
    private final boolean ham;
    private final boolean cheese;
    private final boolean mushrooms;

    Pizza(boolean tomatoSauce, boolean ham, boolean cheese, boolean mushrooms) {
        this.tomatoSauce = tomatoSauce;
        this.ham = ham;
        this.cheese = cheese;
        this.mushrooms = mushrooms;
    }


    public static Pizza fromOrdinal (int ordinal) {
        Pizza[] values = values();
        for (Pizza pizza : values) {
            if (pizza.ordinal() == ordinal)
                return pizza;
        } return null;
    }

    public String toString() {
        String description = ordinal() + " - " + name() + " - Gradients:";
        if (tomatoSauce) { description += " Tomato Souce";}
        if (ham) { description += ", Ham";}
        if (cheese) { description += ", Cheese";}
        if (mushrooms) { description += ", Mushrooms";}
        return  description;
    }
}
