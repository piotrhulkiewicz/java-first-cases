package ClassesAndObjects.CompanyApp;

public class Company {
    public static void main(String[] args) {


        Employee employee1 = new Employee();
        Employee employee2 = new Employee();
        Employee employee3 = new Employee();
        employee1.firstName = "John";
        employee1.lastName = "Rambo";
        employee1.yearofBirth = 1978;
        employee1.expoInYears = 11;

        String employee1Info = "First Name: " + employee1.firstName + ", Last Name: " + employee1.lastName +
                ", Birth Year: " + employee1.yearofBirth + ", Years of expo: " + employee1.expoInYears;

        employee2.firstName = "Anthony";
        employee2.lastName = "Hopkins";
        employee2.yearofBirth = 1966;
        employee2.expoInYears = 23;

        String employee2Info = "First Name: " + employee2.firstName + ", Last Name: " + employee2.lastName +
                ", Birth Year: " + employee2.yearofBirth + ", Years of expo: " + employee2.expoInYears;

        employee3.firstName = "Scottie";
        employee3.lastName = "Pippen";
        employee3.yearofBirth = 1974;
        employee3.expoInYears = 18;

        String employee3Info = "First Name: " + employee3.firstName + ", Last Name: " + employee3.lastName +
                ", Birth Year: " + employee3.yearofBirth + ", Years of expo: " + employee3.expoInYears;

        System.out.println(employee1Info);
        System.out.println(employee2Info);
        System.out.println(employee3Info);


    }
}
