package ClassesAndObjects.CarShopApp.data;

public class Car {
    private int doors;
    private String brand;
    private String model;
    private String color;
    private String wheelsColor;
    private String tiresColor;

    public Car(String brand, String model, String wheelsColor, String tiresColor, int doors) {
        this.brand = brand;
        this.model = model;
        this.wheelsColor = wheelsColor;
        this.tiresColor = tiresColor;
        this.doors = doors;
    }

    public Car(String brand, String model, String wheelsColor, String tiresColor, int doors, String color) {
        this(brand, model, wheelsColor, tiresColor, doors);
        this.color = color;
    }

    public void setDoors(int doors) {
        this.doors = doors;
    }

    public int getDoors() {
        return doors;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getBrand() {
        return brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel() {
        return model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getWheelsColor() {
        return wheelsColor;
    }

    public void setWheelsColor(String wheelsColor) {
        this.wheelsColor = wheelsColor;
    }

    public String getTiresColor() {
        return tiresColor;
    }

    public void setTiresColor(String tiresColor) {
        this.tiresColor = tiresColor;
    }

    public String getCarInfo() {
        return "Brand: " + getBrand() + ", model: " + getModel() + ", Amount of doors: " + getDoors() +
                ", Color of wheels: " + getWheelsColor() + ", Color of tires: " + getTiresColor();
    }

}