package ClassesAndObjects.CarShopApp.logic;

import ClassesAndObjects.CarShopApp.data.Car;

public class CarShop {
    public static void main(String[] args) {
        Car car1 = new Car("Audi", "A4", "Silver", "Black", 4, "Red" );
        car1.setDoors(5);

        System.out.println("Cars for sale: ");
        System.out.println(car1.getCarInfo());

        boolean isCar = car1 instanceof Car;
        System.out.println(isCar);

    }
}
