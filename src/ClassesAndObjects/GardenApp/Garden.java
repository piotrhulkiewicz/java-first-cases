package ClassesAndObjects.GardenApp;

public class Garden {
    public static void main(String[] args) {


        Plant plant1 = new Plant();
        plant1.name = "Rose";
        plant1.descrition = "Red flower";
        plant1.daylyAmountOfWaterNeeded = 0.2;

        Plant plant2 = new Plant();
        plant2.name = "Tulip";
        plant2.descrition = "Yellow flower";
        plant2.daylyAmountOfWaterNeeded = 0.3;

        Plant plant3 = new Plant();
        plant3.name = "Cactus";
        plant3.descrition = "green plant with spikes";
        plant3.daylyAmountOfWaterNeeded = 0.1;

        double dailyAmountOfWAter = plant1.daylyAmountOfWaterNeeded + plant2.daylyAmountOfWaterNeeded +
                plant3.daylyAmountOfWaterNeeded;
        double weeklyAmountOfWater = dailyAmountOfWAter * 7;
        double AnnualAmountOfWater = dailyAmountOfWAter * 365;

        System.out.println("Daily amount of water needed for all plantes is: " + dailyAmountOfWAter + " litres");
        System.out.println("Weekly amount of water needed for all plantes is: " + weeklyAmountOfWater + " litres");
        System.out.println("Annual amount of water needed for all plantes is: " + AnnualAmountOfWater + " litres");
    }
}
