package AccessSpecifiers.TelephoneApp.logic;

import AccessSpecifiers.TelephoneApp.data.Telephone;

public class Charger {


    public void Charge (Telephone telephone) {
        System.out.println("Telephone battery is charging");
        telephone.setChargeLevel(telephone.getChargeLevel()+1);
    }
}
