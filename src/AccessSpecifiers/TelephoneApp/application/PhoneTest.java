package AccessSpecifiers.TelephoneApp.application;

import AccessSpecifiers.TelephoneApp.data.Telephone;
import AccessSpecifiers.TelephoneApp.logic.Charger;

class PhoneTest {
    public static void main(String[] args) {
        Telephone telephone = new Telephone("Samsung", "Galaxy S10", 3400, 75);
        System.out.println(telephone.telInfo());
        Charger charger = new Charger();
        charger.Charge(telephone);
        System.out.println(telephone.telInfo());
    }
}