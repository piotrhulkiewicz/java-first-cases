package AccessSpecifiers.PointApp.logic;


import AccessSpecifiers.PointApp.controller.PointController;
import AccessSpecifiers.PointApp.data.Point;

import java.util.Scanner;


public class PointApplication {
    public static void main(String[] args) {

        int x;
        int y;
        final int addX = 1;
        final int minusX = 2;
        final int addY = 3;
        final int minusY = 4;
        int option;

        Scanner scan = new Scanner(System.in);

        System.out.println("Insert point coordinates: ");
        System.out.println("x = ");
        x = scan.nextInt();
        System.out.println("y = ");
        y = scan.nextInt();

        Point point = new Point(x,y);
        point.pointInfo();

        PointController pointController = new PointController();
        System.out.println("Select an option: ");
        System.out.println("1 - move the point one step to the right");
        System.out.println("2 - move the point one step to the left");
        System.out.println("3 - move the point one step up");
        System.out.println("4 - move the point one step down");

        option = scan.nextInt();

        switch (option) {
            case addX -> pointController.addX(point);
            case minusX -> pointController.minusX(point);
            case addY -> pointController.addY(point);
            case minusY -> pointController.minusY(point);
            default -> System.out.println("incorrect option");
        }
        point.pointInfo();
    }
}