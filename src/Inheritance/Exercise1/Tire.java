package Inheritance.Exercise1;

public class Tire extends Part{

    private double size;
    private double width;

    public Tire(String id, String producer, String model, String serialNumber, double size, double width) {
        setId(id);
        setProducer(producer);
        setModel(model);
        setSerialNumber(serialNumber);
        this.size = size;
        this.width = width;
    }

    public double getSize() {
        return size;
    }

    public void setSize(double size) {
        this.size = size;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void printInfo() {
        System.out.println("Id: " + getId() + ", producer: " + getProducer() + ", model: " + getModel() +
                "serial number: " + getSerialNumber() + ", size: " + size + ", width: " + width);
    }
}
