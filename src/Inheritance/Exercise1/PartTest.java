package Inheritance.Exercise1;

public class PartTest {

    public static void main(String[] args) {

        Tire tire = new Tire("123AA", "Bridgestone","Winter Professional", "1234655687568",
                16.6, 222);

        ExhaustPart exhaustPart = new ExhaustPart(true);
        exhaustPart.setId("124BB");
        exhaustPart.setProducer("Ford");
        exhaustPart.setModel("Loud like hell");
        exhaustPart.setSerialNumber("09876554321");

        tire.printInfo();
        exhaustPart.printInfo();

    }
}
