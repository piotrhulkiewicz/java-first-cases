package Inheritance.Exercise1;

public class ExhaustPart extends Part{

    private boolean isWompatibleWithEuro5;

    public ExhaustPart(boolean isWompatibleWithEuro5) {
        this.isWompatibleWithEuro5 = isWompatibleWithEuro5;
    }

    public boolean isWompatibleWithEuro5() {
        return isWompatibleWithEuro5;
    }

    public void setWompatibleWithEuro5(boolean wompatibleWithEuro5) {
        isWompatibleWithEuro5 = wompatibleWithEuro5;
    }

    public void printInfo() {
        System.out.println("Id: " + getId() + ", producer: " + getProducer() + ", model: " + getModel() +
                "serial number: " + getSerialNumber() + "Is compatible with Euro 5: "  + isWompatibleWithEuro5);
    }
}
