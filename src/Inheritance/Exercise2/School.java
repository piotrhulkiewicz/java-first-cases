package Inheritance.Exercise2;

public class School {

    public static void main(String[] args) {

        Bootcamp bootcamp = new Bootcamp();
        bootcamp.setCourseId("123SD");
        bootcamp.setPrice(1999);
        bootcamp.setName("Kurs java");
        bootcamp.setDescription("Learn core java");
        bootcamp.setTimeInMinutes(3000);
        bootcamp.setEstimatedTimeinMinutes(12000);
        Mentor mentor = new Mentor("Jan", "Kowalski");
        bootcamp.setMentor(mentor);
        bootcamp.setHoursOfMentoring(15);

        bootcamp.printInfo();
    }
}
