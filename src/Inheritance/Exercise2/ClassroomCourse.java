package Inheritance.Exercise2;

public class ClassroomCourse extends Course{

    private String city;
    private int hours;


    public ClassroomCourse(String courseId, double price, String name, String description, String city, int hours) {
        setCourseId(courseId);
        setPrice(price);
        setName(name);
        setDescription(description);
        this.city = city;
        this.hours = hours;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }
}
