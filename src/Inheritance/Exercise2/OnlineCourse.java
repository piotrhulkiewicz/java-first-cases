package Inheritance.Exercise2;

public class OnlineCourse extends Course{

    private int timeInMinutes;
    private int estimatedTimeinMinutes;


    public int getTimeInMinutes() {
        return timeInMinutes;
    }

    public void setTimeInMinutes(int timeInMinutes) {
        this.timeInMinutes = timeInMinutes;
    }

    public int getEstimatedTimeinMinutes() {
        return estimatedTimeinMinutes;
    }

    public void setEstimatedTimeinMinutes(int estimatedTimeinMinutes) {
        this.estimatedTimeinMinutes = estimatedTimeinMinutes;
    }


}
