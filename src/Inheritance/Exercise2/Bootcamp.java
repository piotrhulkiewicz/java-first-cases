package Inheritance.Exercise2;

public class Bootcamp extends OnlineCourse{

    private Mentor mentor;
    private int hoursOfMentoring;

    public Mentor getMentor() {
        return mentor;
    }

    public void setMentor(Mentor mentor) {
        this.mentor = mentor;
    }

    public int getHoursOfMentoring() {

        return hoursOfMentoring;
    }

    public void setHoursOfMentoring(int hoursOfMentoring) {
        this.hoursOfMentoring = hoursOfMentoring;
    }

    public void printInfo(){
        System.out.println("ID: " + getCourseId() + ", price: " + getPrice() + ", name: " + getName() + ", decsription: "
                + getDescription() + ", total time of videos: " + getTimeInMinutes() + ", estimated time of learning "
                + getEstimatedTimeinMinutes());
        System.out.println("Mentor: " + mentor.getFirstName() + " " + mentor.getLastName() +
                ", hours with mentor: " + hoursOfMentoring);
    }
}
