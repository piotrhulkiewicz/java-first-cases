package Loops.ThermostatApp.io;

import Loops.ThermostatApp.model.Thermostat;

import java.util.Scanner;

public class DataReader {

    public Thermostat createThermostat() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter actual temperature");
        double currentTemperature = scanner.nextDouble();
        System.out.println("Enter target temperature");
        double targetTemperature = scanner.nextDouble();
        return new Thermostat(currentTemperature, targetTemperature);
    }
}
