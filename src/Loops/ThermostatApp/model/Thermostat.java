package Loops.ThermostatApp.model;


public class Thermostat {

    private double currentTemperature;
    private final double targetTemperature;


    public Thermostat(double currentTemperature, double targetTemperature) {
        this.currentTemperature = currentTemperature;
        this.targetTemperature = targetTemperature;
    }

    public double getCurrentTemperature() {
        return currentTemperature;
    }

    public double getTargetTemperature() {
        return targetTemperature;
    }

    public void setCurrentTemperature(double currentTemperature) {
        this.currentTemperature = currentTemperature;
    }

    private boolean isTemperatureCorrect() {

        return currentTemperature == targetTemperature;
    }

    public void adjustTemperature() {
        if (!isTemperatureCorrect()) {
            if (getCurrentTemperature() > getTargetTemperature()) {
                while (getCurrentTemperature() - 0.5 > getTargetTemperature()) {
                    setCurrentTemperature(getCurrentTemperature() - 0.5);
                    System.out.println("Actual temperature: " + getCurrentTemperature());
                }
            } else {
                while ((getCurrentTemperature() + 0.5 < getTargetTemperature())) {
                    setCurrentTemperature(getCurrentTemperature() + 0.5);
                    System.out.println("Actual temperature: " + getCurrentTemperature());
                }
            }
            System.out.println("Target temperature reached " + getTargetTemperature() + " degrees");
        }
    }


}

