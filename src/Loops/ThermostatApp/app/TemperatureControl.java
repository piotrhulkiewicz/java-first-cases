package Loops.ThermostatApp.app;

import Loops.ThermostatApp.io.DataReader;
import Loops.ThermostatApp.model.Thermostat;

public class TemperatureControl {

    public static void main(String[] args) {

        DataReader dataReader = new DataReader();
        Thermostat thermostat  = dataReader.createThermostat();

        System.out.println("Current temperature is: " + thermostat.getCurrentTemperature());
        System.out.println("Target temperature is: " + thermostat.getTargetTemperature());
        thermostat.adjustTemperature();

    }
}
