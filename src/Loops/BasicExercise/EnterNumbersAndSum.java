package Loops.BasicExercise;

import java.util.Locale;
import java.util.Scanner;

public class EnterNumbersAndSum {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);
        System.out.println("How many numbers do You want tu add?");
        int amountOfNumbers = scanner.nextInt();

        double sum = 0;
        while (amountOfNumbers > 0) {
            System.out.println("Enter next number");
             sum += scanner.nextDouble();
             amountOfNumbers--;
        }
        System.out.println("Sum of entered numbers is " + sum);
        scanner.close();
    }
}
