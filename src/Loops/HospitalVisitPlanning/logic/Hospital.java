package Loops.HospitalVisitPlanning.logic;

import Loops.HospitalVisitPlanning.model.Patient;

import java.util.Scanner;

public class Hospital {
    Scanner sc = new Scanner(System.in);
    Patient[] patients = new Patient[2];
    int numberOfPatient = 0;
    final int exit = 0;
    final int addPatient = 1;
    final int printPatientList = 2;

    public void addPatient() {
            if (numberOfPatient < patients.length) {
                patients[numberOfPatient] = createPatient();
                numberOfPatient++;
            } else System.out.println("patient limit exhausted");
        }


    private Patient createPatient() {
        System.out.println("Enter firstname ");
        String firstname = sc.nextLine();
        System.out.println("Enter lastname ");
        String lastname = sc.nextLine();
        System.out.println("Enter personID ");
        String personID = sc.nextLine();
        return new Patient(firstname, lastname, personID);
    }

    public void patientsInfo() {
        for (Patient patient : patients) {
            if (patient != null) {
                System.out.println("Patient data: " + patient.getFirstName() + " " + patient.getLastName() + " "
                        + patient.getPersonID());
            }
        }
    }

    public void pacientListController() {
        int option;
        do {
            System.out.println("Select an option ");
            System.out.println(exit + " - exit program");
            System.out.println(addPatient + " - add patient to list");
            System.out.println(printPatientList + " - print patients list");
            option = sc.nextInt();
            sc.nextLine();
            switch (option) {
                case exit -> System.out.println("End of program");
                case addPatient -> addPatient();
                case printPatientList -> patientsInfo();
            }
        } while (option != exit);
    }
    public void close() {
        sc.close();
    }

}
