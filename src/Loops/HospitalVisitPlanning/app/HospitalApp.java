package Loops.HospitalVisitPlanning.app;

import Loops.HospitalVisitPlanning.logic.Hospital;

public class HospitalApp {
    public static void main(String[] args) {

        Hospital hospital = new Hospital();
        hospital.pacientListController();
        hospital.close();
    }
}
