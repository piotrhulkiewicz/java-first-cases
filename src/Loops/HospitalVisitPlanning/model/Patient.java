package Loops.HospitalVisitPlanning.model;

public class Patient {

    private String firstName;
    private String lastName;
    private String personID;

    public Patient (String firstName, String lastName, String personID ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.personID = personID;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPersonID() {
        return personID;
    }

    public void setPersonID(String personID) {
        this.personID = personID;
    }
}
