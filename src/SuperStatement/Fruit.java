package SuperStatement;

public class Fruit {

    private int weightInGrams;
    private String fruitType;

    public Fruit (int weightInGrams, String fruitType) {
        this.weightInGrams = weightInGrams;
        this.fruitType = fruitType;
    }

    public void setWeightInGrams(int weightInGrams) {
        this.weightInGrams = weightInGrams;
    }

    public void setFruitType(String fruitType) {
        this.fruitType = fruitType;
    }

    public int getWeightInGrams() {
        return weightInGrams;
    }

    public String getFruitType() {
        return fruitType;
    }

    public String getInfo() {
        return weightInGrams + ", " + fruitType;
    }
}

