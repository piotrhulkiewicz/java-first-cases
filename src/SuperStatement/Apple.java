package SuperStatement;

public class Apple extends Fruit{

    private String fruitVariety;
    public final static String TYPE = "applewood";

    public Apple(int weightInGrams, String fruitVariety) {
        super(weightInGrams, TYPE);
        this.fruitVariety = fruitVariety;
    }

    @Override
    public String getInfo() {
        return super.getInfo() + ", " + fruitVariety;
    }
}
