package ObjectClassMethods.ComputerStore;

public class DataStore {

    private static final int MAX_COMPUTERS = 100;
    private Computer[] computers = new Computer[MAX_COMPUTERS];
    private int currentComputerNumber;

    public void add(Computer computer) {
        if (currentComputerNumber == computers.length) {
            System.out.println("Max limit of computers in the store reached");
        } else if (computer != null) {
            computers[currentComputerNumber] = computer;
            currentComputerNumber++;
        }
    }

    public void printComputers() {
        System.out.println("Computers available in the store: ");
        for (Computer computer : computers) {
            if (computer != null) {
                System.out.println(computer);
            }
        }

    }

    public String checkAvailability(Computer computerToCompare) {
        int counter = 0;
        for (int i = 0; i < currentComputerNumber; i++) {
            if (computerToCompare.equals(computers[i]))
                counter++;
        } return "There is " + counter + " computer like " + computerToCompare.toString() + " in the store";
    }
}
