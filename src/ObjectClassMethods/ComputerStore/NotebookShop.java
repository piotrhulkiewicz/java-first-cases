package ObjectClassMethods.ComputerStore;

public class NotebookShop {

    public static void main(String[] args) {

        Computer comp1 = new Computer("IBM", 23);
        Computer comp2 = new Computer("Apple", 454);
        Computer comp3 = new Computer("HP", 123);
        Computer comp4 = new Computer("HP", 123);
        Computer comp5 = new Computer("Lenovo", 6456);

        DataStore dataStore = new DataStore();

        dataStore.add(comp1);
        dataStore.add(comp2);
        dataStore.add(comp3);
        dataStore.add(comp4);
        dataStore.add(comp5);

        System.out.println(dataStore.checkAvailability(new Computer("HP", 123)));

        dataStore.printComputers();
    }
}
