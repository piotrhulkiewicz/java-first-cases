package Polymorphism.Basics;

import java.util.Scanner;

public class Zoo {
    public static void main(String[] args) {

        Animal animal1 = new Animal("Animal");
        Animal dog = new Dog("Burek");
        Animal cat = new Cat("Mruczek");

        System.out.println(animal1.giveSound());
        System.out.println(dog.giveSound());
        System.out.println(cat.giveSound());

        Animal[] animals = new Animal[3];
        animals[0] = animal1;
        animals[1] = dog;
        animals[2] = cat;

        for (Animal animal : animals) {
            System.out.println(animal.giveSound());
        }

        changeAnimalName(animals[2], "Fredro");

        System.out.println(animals[2].giveSound());

        if (dog instanceof Dog) {
            Dog specifiedDog = (Dog) dog;
            specifiedDog.bark();
        }
        ((Cat)cat).meow(); // 2 ways to project types


        Animal animal2 = null;
        Scanner scan = new Scanner(System.in);
        System.out.println("Choose the animal:");
        System.out.println("1 - dog, 2 - cat");
        int option = scan.nextInt();
        if (option == 1) {
            animal2 = new Dog("Smurf");
        }    else if (option == 2) {
            animal2 = new Cat("Little");
        }

        if (animal2 instanceof Dog) {
            ((Dog) animal2).bark();
        } else {
            assert animal2 != null;
            ((Cat) animal2).meow();
        }


        }

    public static void changeAnimalName (Animal animal, String newName) {
        animal.setName(newName);
    }


}
