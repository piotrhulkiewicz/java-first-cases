package Polymorphism.Basics;

public class Animal {

    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String giveSound() {
        return "I am an animal and my name is: " + name;
    }
}
