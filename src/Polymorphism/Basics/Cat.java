package Polymorphism.Basics;

public class Cat extends Animal{

    private String name;

    public Cat (String name) {
        super(name);
    }

    @Override
    public String giveSound() {
        return "I am a cat and my name is: " + getName();
    }

    public void meow() {
        System.out.println("meow, meow");
    }
}
