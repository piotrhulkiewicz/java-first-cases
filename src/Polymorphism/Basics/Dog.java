package Polymorphism.Basics;


public class Dog extends Animal {

    private String name;

    public Dog(String name) {
        super(name);
    }

    @Override
    public String giveSound() {
        return "I am a dog and my name is: " + getName();
    }

    public void bark() {
        System.out.println("How, how");
    }
}

