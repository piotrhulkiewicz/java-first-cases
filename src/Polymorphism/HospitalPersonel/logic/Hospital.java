package Polymorphism.HospitalPersonel.logic;

import Polymorphism.HospitalPersonel.model.Doctor;
import Polymorphism.HospitalPersonel.model.Nurse;
import Polymorphism.HospitalPersonel.model.Person;

public class Hospital {

    private final int maxPersonnel = 10;
    private int currentlyEmployed = 0;
    Person[] staff = new Person[maxPersonnel];

    public void addPerson(Person person) {
        if (currentlyEmployed < staff.length) {
            staff[currentlyEmployed] = person;
            currentlyEmployed++;
        }
    }

    public void getStaffInfo (){
        for (Person person : staff) {
            if (person instanceof Nurse) {
                System.out.println("First name: " + person.getFirstName() + ", last name: " + person.getLastName() +
                        ", salary" + ", overtime: " + ((Nurse)person).getOvertime());
            } else if (person instanceof Doctor) {
                System.out.println("First name: " + person.getFirstName() + ", last name: " + person.getLastName() +
                        ", salary" + ", bonus: " + ((Doctor)person).getBonus());
            }

        }
    }
}
