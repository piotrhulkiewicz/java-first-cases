package Polymorphism.HospitalPersonel.model;

public class Nurse extends Person{

    private int overtime;

    public Nurse (String firstName, String lastname, double salary, int overtime) {
        super(firstName, lastname, salary);
        this.overtime = overtime;
    }

    public void setOvertime(int overtime) {
        this.overtime = overtime;
    }

    public int getOvertime() {
        return overtime;
    }
}
