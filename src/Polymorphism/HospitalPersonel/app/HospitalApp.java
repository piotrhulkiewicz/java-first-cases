package Polymorphism.HospitalPersonel.app;

import Polymorphism.HospitalPersonel.logic.Hospital;
import Polymorphism.HospitalPersonel.model.Doctor;
import Polymorphism.HospitalPersonel.model.Nurse;
import Polymorphism.HospitalPersonel.model.Person;

public class HospitalApp {

    public static void main(String[] args) {

        Person doctor = new Doctor("John", "Rambo", 10000, 3000);
        Person nurse1 = new Nurse("Maria", "Antonina", 2500, 10);
        Person nurse2 = new Nurse("Ann", "Apple", 3000, 20);

        Hospital hospital = new Hospital();
        hospital.addPerson(doctor);
        hospital.addPerson(nurse1);
        hospital.addPerson(nurse2);
        hospital.getStaffInfo();
    }
}
