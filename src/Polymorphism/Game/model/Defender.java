package Polymorphism.Game.model;

public class Defender extends GameCharacter {

    private double defenceBonus;


    public Defender(String name, int attackPower, int defencePower, int healthPoints, double defenceBonus) {
        super(name, attackPower, defencePower, healthPoints);
        this.defenceBonus = defenceBonus;
    }

    public double getDefenceBonus() {
        return defenceBonus;
    }

    public void setDefenceBonus(double defenceBonus) {
        this.defenceBonus = defenceBonus;
    }
    @Override
    public int getDefencePower() {
        return (int)(super.getDefencePower() + super.getDefencePower() * defenceBonus);
    }
}
