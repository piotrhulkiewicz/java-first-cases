package Polymorphism.Game.model;

public class Attacker extends GameCharacter {

    private double attackBonus;

    public Attacker (String name, int attackPower, int defencePower, int healthPoints, double attackBonus) {
        super(name, attackPower, defencePower, healthPoints);
        this.attackBonus = attackBonus;
    }

    public void setAttackBonus(double attackBonus) {
        this.attackBonus = attackBonus;
    }

    public double getAttackBonus() {
        return attackBonus;
    }
    @Override
    public int getAttackPower() {

        return (int)(super.getAttackPower() + super.getAttackPower() * attackBonus);
    }
}
