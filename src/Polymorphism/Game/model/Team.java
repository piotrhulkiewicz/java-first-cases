package Polymorphism.Game.model;

public class Team {

    private final int maxCharactersInTeam = 3;
    private GameCharacter[] team = new GameCharacter[maxCharactersInTeam];
    private int totalHealthPoints;

    public Team(GameCharacter gameCharakter1, GameCharacter gameCharakter2, GameCharacter gameCharakter3) {
        team[0] = gameCharakter1;
        team[1] = gameCharakter2;
        team[2] = gameCharakter3;
        this.totalHealthPoints = team[0].getHealthPoints() + team[1].getHealthPoints() + team[2].getHealthPoints();
    }


    public int getTotalAttack() {
        int totalAttack = 0;
        for (GameCharacter gameCharakter : team) {
            totalAttack += gameCharakter.getAttackPower();
        }
        return  totalAttack;
    }

    public int getTotalDefence() {
        int totalDefence = 0;
        for (GameCharacter gameCharakter : team) {
            totalDefence += gameCharakter.getDefencePower();
        }
        return  totalDefence;
    }

    public int getTotalHealthPoints() {
        int totalHealthPoints = 0;
        for (GameCharacter gameCharakter : team) {
            totalHealthPoints += gameCharakter.getHealthPoints();
        }
        return  totalHealthPoints;
    }

    public void setTotalHealthPoints(int totalHealthPoints) {
        this.totalHealthPoints = totalHealthPoints;
    }

    public void getTeamInfo() {
        System.out.println("Team Total Attack: " + getTotalAttack() + ", Team Total Defence: " + getTotalDefence() +
                ", team Total Health Points: " + totalHealthPoints);
    }
}
