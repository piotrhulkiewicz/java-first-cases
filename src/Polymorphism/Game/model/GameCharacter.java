package Polymorphism.Game.model;

import java.util.Objects;

public class GameCharacter {

    private String name;
    private int attackPower;
    private int defencePower;
    private int healthPoints;

    public GameCharacter(String name, int attackPower, int defencePower, int healthPoints) {
        this.name = name;
        this.attackPower = attackPower;
        this.defencePower = defencePower;
        this.healthPoints = healthPoints;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAttackPower() {
        return attackPower;
    }

    public void setAttackPower(int attackPower) {
        this.attackPower = attackPower;
    }

    public int getDefencePower() {
        return defencePower;
    }

    public void setDefencePower(int defencePower) {
        this.defencePower = defencePower;
    }

    public int getHealthPoints() {
        return healthPoints;
    }

    public void setHealthPoints(int healthPoints) {
        this.healthPoints = healthPoints;
    }


}
