package Polymorphism.Game.logic;

import Polymorphism.Game.model.Team;

public class BattleSimulator {

    public void battle(Team team1, Team team2) {
        if (team1.getTotalAttack() >= team2.getTotalDefence()) {
            team2.setTotalHealthPoints(team2.getTotalHealthPoints() - team1.getTotalAttack());
        }
        if (team2.getTotalAttack() >= team1.getTotalDefence()) {
            team1.setTotalHealthPoints(team1.getTotalHealthPoints() - team2.getTotalAttack());
        }
        printBattleInfo(team1, team2);
    }

    private void printBattleInfo(Team team1, Team team2) {
        if (team1.getTotalHealthPoints() > team2.getTotalHealthPoints()) {
            System.out.println("Team 1 has won the battle");
        } else if (team2.getTotalHealthPoints() > team1.getTotalHealthPoints()) {
            System.out.println("Team 2 has won the battle");
        } else System.out.println("Nobody won");
    }
}
