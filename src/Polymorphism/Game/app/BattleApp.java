package Polymorphism.Game.app;

import Polymorphism.Game.logic.BattleSimulator;
import Polymorphism.Game.model.Attacker;
import Polymorphism.Game.model.Defender;
import Polymorphism.Game.model.GameCharacter;
import Polymorphism.Game.model.Team;

public class BattleApp {

    public static void main(String[] args) {


        GameCharacter character1 = new Attacker("John", 70, 40, 100, 0.2);
        GameCharacter character2 = new Attacker("John", 70, 40, 80, 0.2);
        GameCharacter character3 = new Defender("John", 70, 40, 80, 0.2);
        GameCharacter character4 = new Defender("John", 70, 40, 80, 0.2);
        GameCharacter character5 = new Defender("John", 70, 40, 80, 0.2);
        GameCharacter character6 = new Attacker("John", 70, 40, 80, 0.2);

        Team team1 = new Team(character1, character2, character3);
        Team team2 = new Team(character4, character5, character6);

        team1.getTeamInfo();
        team2.getTeamInfo();

        BattleSimulator battleSimulator = new BattleSimulator();
        battleSimulator.battle(team1, team2);

    }
}
