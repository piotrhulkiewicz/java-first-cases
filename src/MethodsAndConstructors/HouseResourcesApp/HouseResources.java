package MethodsAndConstructors.HouseResourcesApp;

public class HouseResources {
    double amountOfWater;
    double amountOfFuel;

    HouseResources (double water, double fuel) {
        amountOfWater = water;
        amountOfFuel = fuel;
    }

    void takeShower() {
        amountOfWater -= 48;
    }

    void takeBath() {
        amountOfWater -=  86;
    }

    void makeDinner() {
        amountOfWater -=  4;
        amountOfFuel -= 0.1;
    }

    void boilWater() {
        amountOfWater -= 0.5;
        amountOfFuel -= 0.1;
    }

    void watchTv(int hours) {
        amountOfFuel = amountOfFuel - (hours * 0.06);
    }

    String resourcesInfo() {
        return "Amount of water in the house: " + amountOfWater + ", amount of fuel in the house: " + amountOfFuel;
    }
}
