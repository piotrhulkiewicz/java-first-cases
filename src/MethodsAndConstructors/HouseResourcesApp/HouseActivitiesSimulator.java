package MethodsAndConstructors.HouseResourcesApp;

public class HouseActivitiesSimulator {
    public static void main(String[] args) {
        double amountOfWater = 10_000;
        double amountofFuel = 1_000;
        HouseResources houseResources = new HouseResources(amountOfWater, amountofFuel);
        System.out.println(houseResources.resourcesInfo());

        System.out.println("Taking shower");
        houseResources.takeShower();
        System.out.println(houseResources.resourcesInfo());

        System.out.println("Taking Bath");
        houseResources.takeBath();
        System.out.println(houseResources.resourcesInfo());

        System.out.println("Make dinner");
        houseResources.makeDinner();
        System.out.println(houseResources.resourcesInfo());

        System.out.println("Boil water");
        houseResources.boilWater();
        System.out.println(houseResources.resourcesInfo());

        System.out.println("watch TV for 3 ours");
        houseResources.watchTv(3);
        System.out.println(houseResources.resourcesInfo());

    }
}
