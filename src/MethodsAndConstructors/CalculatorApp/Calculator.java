package MethodsAndConstructors.CalculatorApp;

public class Calculator {
    public static void main(String[] args) {

        double x = 10.7;
        double y = 0.5;

        Calculate calc = new Calculate();
        System.out.println("x = " + x + ", y = " + y);
        System.out.println("x + y = " + calc.add(x,y));
        System.out.println("x - y = " + calc.subtract(x,y));
        System.out.println("x * y = " + calc.mulitiply(x,y));
        System.out.println("x / y = " + calc.divide(x,y));

    }


}
