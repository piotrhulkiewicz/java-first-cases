package MethodsAndConstructors.ConverterApp;

public class DistanceConverter {
    double metresToCentimetres(double metres) {
        return metres * 100;
    }

    double metresToMilliMetres(double metres) {
        return metres * 10_000;
    }
    double centimetresToMetres(double centimetres) {
        return centimetres / 100;
    }
    double millimetersToMetres(double millimetres) {
        return millimetres / 10_000;
    }
}
