package MethodsAndConstructors.ConverterApp;

public class Convert {

    public static void main(String[] args) {

        double metres = 5.5;
        int hours = 4;

        TimeConverter time = new TimeConverter();
        DistanceConverter distance = new DistanceConverter();

        int minutes = time.hoursToMinutes(hours);
        int seconds = time.minutesToSeconds(minutes);
        int milliseconds = time.secondsToMillisecond(seconds);
        System.out.println("4 hours is " + minutes + " minutes, which is " + seconds + " seconds, which is " +
                milliseconds + " milliseconds.");

        System.out.println("5.5 metres is " + distance.metresToMilliMetres(metres) + " millimetres");
    }
}
