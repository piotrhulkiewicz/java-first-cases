package MethodsAndConstructors.ShopApp;

class Offer {
    Product product;
    double price;
    boolean special;

    Offer (Product prod, double pric, boolean spec) {
        product = prod;
        price = pric;
        special = spec;
    }

    void offerInfo () {
        System.out.println(product.name + " "
                + product.producer + " "
                + price + "zł, "
                + "oferta specjalna? " + special);
    }
}