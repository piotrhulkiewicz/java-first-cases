package MethodsAndConstructors.ShopApp;

class Product {
    String name;
    String producer;

    Product (String n, String p) {
        name = n;
        producer = p;
    }

}