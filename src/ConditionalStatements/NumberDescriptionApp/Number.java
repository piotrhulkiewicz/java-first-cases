package ConditionalStatements.NumberDescriptionApp;

import java.util.Random;

public class Number {
    public static void main(String[] args) {

        Random random = new Random();
        int number = random.nextInt(10000);
        NumberUtils numberUtils = new NumberUtils();

        System.out.println("The drawn number is: " + number);
        numberUtils.compareNumberToAnother(number, 2000);
        numberUtils.evenOrOdd(number);
        System.out.println("Completed to 4 digits: " + numberUtils.completeTo4Digits(number));
    }
}


