package ConditionalStatements.NumberDescriptionApp;

public class NumberComparision {
    public static void main(String[] args) {


        int a = 35;
        int b = 20;
        int c = 30;

        if (a > b) {
            if (a > c) {
                System.out.println("A is bigger than b and c");
            } else System.out.println("A is bigger than b but lower than c");
        } else System.out.println("A is lower than b");

    }


}
