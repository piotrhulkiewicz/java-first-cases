package ConditionalStatements.NumberDescriptionApp;

public class NumberUtils {
    public void compareNumberToAnother(int number, int compareTo) {
        if (number < compareTo) {
            System.out.println(number + " is smaller than " + compareTo);
        } else if (number == compareTo) {
            System.out.println(number + " equals " + compareTo);
        } else System.out.println(number + " is bigger than "+ compareTo);
    }

    public void evenOrOdd(int number) {
        if (number % 2 == 0) {
            System.out.println(number + " is even number");
        } else {
            System.out.println(number + " is odd number");
        }
    }

    public int completeTo4Digits(int number) {
        if (number < 1000 && number > 99) {
            return number * 10;
        } else if (number < 100 && number > 9) {
            return  number * 100;
        } else if (number < 10 && number > 0) {
            return  number * 1000;
        } else {
            return number;
        }

    }
}
