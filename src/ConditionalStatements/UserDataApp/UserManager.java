package ConditionalStatements.UserDataApp;

import java.util.Scanner;

public class UserManager {
    public static void main(String[] args) {
        User user = new User("Alice", "Spring");

        Scanner scan = new Scanner(System.in);


        System.out.println("Select an option: ");
        System.out.println("0 - finish the program");
        System.out.println("1 - print user information");
        System.out.println("2 - Modify and print user information");

        int option = scan.nextInt();

        if (option == 0) {
            System.out.println("End program");
        } else if (option == 1) {
            System.out.println("User name: " + user.getFirstName() + " " + user.getLastName());
        } else if (option == 2) {
            scan.nextLine();
            System.out.println("insert new firstname ");
            user.setFirstName(scan.nextLine());
            System.out.println("insert new lastname ");
            user.setLastName(scan.nextLine());
            System.out.println("New user name: " + user.getFirstName() + " " + user.getLastName());
        } else System.out.println("incorrect option");

        System.out.println("Select an option: ");
        System.out.println("0 - finish the program");
        System.out.println("1 - print user information");
        System.out.println("2 - Modify and print user information");

        option = scan.nextInt();

        switch (option) {
            case 0 -> System.out.println("End program");
            case 1 -> System.out.println("User name: " + user.getFirstName() + " " + user.getLastName());
            case 2 -> {
                scan.nextLine();
                System.out.println("insert new firstname ");
                user.setFirstName(scan.nextLine());
                System.out.println("insert new lastname ");
                user.setLastName(scan.nextLine());
                System.out.println("New user name: " + user.getFirstName() + " " + user.getLastName());
            }default -> System.out.println("incorrect option");
        }
    }

}
