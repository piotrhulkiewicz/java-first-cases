package Encapsulation.CinemaTicketingApp.app;

import Encapsulation.CinemaTicketingApp.logic.TicketSellController;
import Encapsulation.CinemaTicketingApp.model.Movie;
import Encapsulation.CinemaTicketingApp.model.Person;
import Encapsulation.CinemaTicketingApp.model.Ticket;

public class CinemaSystem {
    public static void main(String[] args) {

        int maxSeats = 72;
        int ticketId = 0;
        int freeSeats = maxSeats - ticketId;

        Movie movie = new Movie("Omen", "Horror", 128, 16, maxSeats, freeSeats, 0);
        Person person1 = new Person("John", "Rambo", 15);
        Person person2 = new Person("Bilbo", "Baggins", 33);
        Person person3 = new Person("Pippin", "Tuk", 26);

        TicketSellController ticketSellController = new TicketSellController();
        Ticket ticket1 = ticketSellController.TicketSeller(movie, person1, ticketId);
        Ticket ticket2 = ticketSellController.TicketSeller(movie, person2, ticketId);
        Ticket ticket3 = ticketSellController.TicketSeller(movie, person3, ticketId);

        System.out.println("Sprzedane bilety");
        ticketSellController.cinemainfo(ticket1);
        ticketSellController.cinemainfo(ticket2);
        ticketSellController.cinemainfo(ticket3);

        System.out.println("Liczba pozostałych miejsc: " + movie.getFreeSeats());
        System.out.println("Liczba sprzedanych biletów: " + (movie.getMaxSeats() - movie.getFreeSeats()));
    }
}