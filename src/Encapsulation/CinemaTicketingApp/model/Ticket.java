package Encapsulation.CinemaTicketingApp.model;

public class Ticket {

    private int ticketID;
    private Person person;
    private Movie movie;

    public Ticket (int ticketID, Person person, Movie movie) {
        this.ticketID = ticketID;
        this.person = person;
        this.movie = movie;
    }

    public int getTicketID() {
        return ticketID;
    }

    public void setTicketID(int ticketID) {
        this.ticketID = ticketID;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }
}
