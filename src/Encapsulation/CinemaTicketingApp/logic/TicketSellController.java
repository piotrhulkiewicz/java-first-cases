package Encapsulation.CinemaTicketingApp.logic;

import Encapsulation.CinemaTicketingApp.model.Movie;
import Encapsulation.CinemaTicketingApp.model.Person;
import Encapsulation.CinemaTicketingApp.model.Ticket;

public class TicketSellController {


    public Ticket TicketSeller (Movie movie, Person person, int ticketId) {
        if (movie.getFreeSeats()== 0) {
            System.out.println("Brak wolnych miejsc na seans");
            return null;
        } else if (person.getAge() < movie.getAgeRequired()) {
            System.out.println("Film dostępny dla osób powyżej " + movie.getAgeRequired() + " lat");
            return null;
        } else {
            movie.setSoldTickets(movie.getSoldTickets() + 1);
            ticketId = movie.getSoldTickets();
            movie.setFreeSeats(movie.getFreeSeats() - 1);
            return new Ticket(ticketId, person, movie) {
            };
        }

    }
        public void cinemainfo(Ticket ticket) {
            if (ticket != null) {
                System.out.println(ticket.getTicketID() + " | " + ticket.getPerson().getFirstName() +
                        " " + ticket.getPerson().getLastName() + " | " + ticket.getMovie().getMovieTitle() + " - "
                        + ticket.getMovie().getMovieType() + " - " + ticket.getMovie().getMovieTime() + "min");
        }
    }
}


