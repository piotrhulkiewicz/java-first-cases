package Encapsulation.DiscountApp.logic;

import Encapsulation.DiscountApp.model.Client;

public class ClientInfo {

    public void clientAndPriceInfo(Client client, double price, double discountPrice) {
        if (client.getFirstName() == null) {
            if (client.getLastName() != null) {
                System.out.println("Good day mr./ms. " + client.getLastName());
            } else {
                System.out.println("Hello new client");
            }
        } else if (client.getLastName() == null) {
            System.out.println("Hello " + client.getFirstName());
            } else {
                System.out.println("Hello " + client.getFirstName() + " " + client.getLastName());
        }
        System.out.println("Price without discount: " + price);
        System.out.println("Price with discount: " + discountPrice);
    }
}
