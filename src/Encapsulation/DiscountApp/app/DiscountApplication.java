package Encapsulation.DiscountApp.app;

import Encapsulation.DiscountApp.logic.ClientInfo;
import Encapsulation.DiscountApp.logic.DiscountService;
import Encapsulation.DiscountApp.model.Client;

public class DiscountApplication {
    public static void main(String[] args) {

        Client client1 = new Client("John", "Rambo", true);
        double price = 1100; //zł

        Client client2 = new Client(null, "Baggins", false);
        double price2 = 1100; //zł

        DiscountService discountService = new DiscountService();
        double discountPrice = discountService.calculateDiscountPrice(client1, price);
        double discountPrice2 = discountService.calculateDiscountPrice(client2, price2);

        ClientInfo clientInfo = new ClientInfo();
        clientInfo.clientAndPriceInfo(client1, price, discountPrice);
        clientInfo.clientAndPriceInfo(client2, price2, discountPrice2);
    }

}
