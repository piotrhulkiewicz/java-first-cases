package Encapsulation.DiscountApp.model;

public class Client {
    String firstName;
    String lastName;
    boolean isPremium;

    public Client(String firstName, String lastName, boolean isPremium) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.isPremium = isPremium;
    }

    public void lastFirstName (String lastName) {
        this.lastName = lastName;
    }

    public void setPremium (boolean isPremium) {
        this.isPremium = isPremium;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public boolean isPremium() {
        return isPremium;
    }
}
