package Arrays.ElevatorSimulatorApp.app;

import Arrays.ElevatorSimulatorApp.logic.Elevator;
import Arrays.ElevatorSimulatorApp.model.Person;

public class ElevatorTest {

    public static void main(String[] args) {

        Person person1 = new Person(1, 70);
        Person person2 = new Person(2, 100);
        Person person3 = new Person(3, 90);
        Person person4 = new Person(4, 150);
        Person person5 = new Person(5, 110);

        Person[] passengers = new Person[4];

        Elevator elevator = new Elevator(passengers, 0);

        elevator.add(person1);
        elevator.add(person2);
        elevator.add(person3);
        elevator.start();
        elevator.add(person4);
        elevator.start();
        elevator.add(person5);
        elevator.start();
        elevator.emptyingTheElevator();
        elevator.start();
    }
}
