package Arrays.ElevatorSimulatorApp.model;

public class Person {
    private int personID;
    private int weight;

    public Person (int personID, int weight) {
        this.personID = personID;
        this.weight = weight;
    }

    public void setPersonID(int personID) {
        this.personID = personID;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getPersonID() {
        return personID;
    }

    public int getWeight() {
        return weight;
    }
}
