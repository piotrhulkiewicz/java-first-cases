package Arrays.ElevatorSimulatorApp.logic;

import Arrays.ElevatorSimulatorApp.model.Person;

public class Elevator {
    private Person[] passengers = new Person[4];
    private int numberOfPassengers = 0;
    private final int maxWeight = 400;

    public Elevator(Person[] passengers, int numberOfPassengers) {
        this.passengers = passengers;
        this.numberOfPassengers = numberOfPassengers;
    }

    public Person[] getPassengers() {
        return passengers;
    }

    public void setPassengers(Person[] passengers) {
        this.passengers = passengers;
    }

    public int getNumberOfPassengers() {
        return numberOfPassengers;
    }

    public void setNumberOfPassengers(int numberOfPassengers) {
        this.numberOfPassengers = numberOfPassengers;
    }

    public void add(Person person) {

        if (getNumberOfPassengers() < passengers.length) {
            passengers[getNumberOfPassengers()] = person;
            setNumberOfPassengers(getNumberOfPassengers() + 1);
        } else {
            System.out.println("To many passengers");
        }
    }

    public void start() {

        if (weightIsOK())
            System.out.println("The elevator has started " + addPassengersWeight() + " kg");
        else
            System.out.println("The elevator is overloaded by " + (addPassengersWeight() - maxWeight) + " kg.");
    }

    private boolean weightIsOK() {
        return addPassengersWeight() <= maxWeight;
    }

    public int addPassengersWeight() {
        int passengersWeight = 0;
        if (passengers[0] != null) passengersWeight += passengers[0].getWeight();
        if (passengers[1] != null) passengersWeight += passengers[1].getWeight();
        if (passengers[2] != null) passengersWeight += passengers[2].getWeight();
        if (passengers[3] != null) passengersWeight += passengers[3].getWeight();

    return passengersWeight;
    }

    public void emptyingTheElevator() {
        passengers[0] = null;
        passengers[1] = null;
        passengers[2] = null;
        passengers[3] = null;
        numberOfPassengers = 0;
        System.out.println("Elevator is empty");
    }

}
