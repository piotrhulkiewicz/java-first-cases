package Arrays.ArraysInitiation;

public class ArraysBasicExercise {
    public static void main(String[] args) {

        int[] numbers1 = {1, 2, 3};
        int[] numbers2 = new int[3];
        numbers2[0] = 4;
        numbers2[1] = 5;
        numbers2[2] = 6;
        int  numbers1Sum = numbers1[0] + numbers1[1] + numbers1[2];
        int  numbers2Sum = numbers2[0] + numbers2[1] + numbers2[2];


        System.out.println("Sum of all numbers in both arrays = " + (numbers1Sum + numbers2Sum));

        ArraysUtils arraysUtils = new ArraysUtils();
        System.out.println("Sum of all numbers in both arrays = " + arraysUtils.ArraysAdd(numbers1, numbers2));
    }


}
