package Arrays.ArraysInitiation;

public class MultiDimensionalArrays {

    public static void main(String[] args) {

        double[][] multiArray = new double[3][3];

        double[] array1 = {1.0, 1.5, 2.0};
        double[] array2 = {1.5, 2.0, 2.5};
        double[] array3 = {2.0, 2.5, 3.0};

        multiArray[0] = array1;
        multiArray[1] = array2;
        multiArray[2] = array3;

        double sumMultipliesDiagonals = multiArray[0][0] * multiArray[1][1] * multiArray[2][2] +
                                        multiArray[0][2] * multiArray[1][1] * multiArray[2][0];
        double productSumArraysMiddleColumnAndRow = (multiArray[0][1] + multiArray[1][1] + multiArray[2][1]) *
                                                    (multiArray[1][0] + multiArray[1][1] + multiArray[1][2]);
        double sumAllElementsExceptMiddle = multiArray[0][0] + multiArray[1][0] + multiArray[2][0] + multiArray[0][1] +
                                            multiArray[2][1] + multiArray[0][2] + multiArray[1][2] + multiArray[2][2];
        System.out.println("Sum of products array's diagonals " +sumMultipliesDiagonals);
        System.out.println("Product of sums of the array's middle column and middle row:  "
                + productSumArraysMiddleColumnAndRow);
        System.out.println("Sum of all elements except the middle one " + sumAllElementsExceptMiddle);
    }
}
