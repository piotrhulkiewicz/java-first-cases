package AggregationAndComposition;

public class Bank {
    public static void main(String[] args) {
        Adress adress1 = new Adress();
        adress1.city = "Philadelphia";
        adress1.zipCode = "45-667";
        adress1.street = "Main Street";
        adress1.houseNumber = "123A";
        adress1.flatNumber = 55;

        Adress adress2 = new Adress();
        adress2.city = "Boston";
        adress2.zipCode = "97-617";
        adress2.street = "Grove Street";
        adress2.houseNumber = "273";
        adress2.flatNumber = 11;

        Person person1 = new Person();
        person1.firstName = "John";
        person1.lastName = "Rambo";
        person1.IdentityNumber = "791206062384";
        person1.correspondenceAdress = adress1;
        person1.livingAdress = adress2;

        Person person2 = new Person();
        person2.firstName = "Johny";
        person2.lastName = "Wick";
        person2.IdentityNumber = "5464166062384";
        person2.correspondenceAdress = adress2;
        person2.livingAdress = adress2;

        Account account1 = new Account();
        account1.owner = person1;
        account1.balance = 45_123.80;

        Account account2 = new Account();
        account2.owner = person2;
        account2.balance = 34_653.00;

        Credit credit1 = new Credit();
        credit1.borrower = person1;
        credit1.cashBorrowed = 10000;
        credit1.cashReturned = 1000;
        credit1.interestRate = 0.10;
        credit1.termMonths = 10;

        System.out.println("Client 1: ");
        System.out.println(person1.firstName + " " + person1.lastName + ", Identity number: " + person1.IdentityNumber );
        System.out.println("living in: " + person1.livingAdress.city);
        System.out.println("account balance: " + account1.balance);
        System.out.println("cash to return: " + (credit1.cashBorrowed - credit1.cashReturned));

        System.out.println("Client 2: ");
        System.out.println(person2.firstName + " " + person2.lastName + ", Identity number: " + person2.IdentityNumber );
        System.out.println("living in: " + person2.livingAdress.city);
        System.out.println("account balance: " + account2.balance);

    }
}
