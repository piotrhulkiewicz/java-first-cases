package AggregationAndComposition;

public class AuctionApp {
    public static void main(String[] args) {


        Auction auction1 = new Auction();
        auction1.title = "Xbox One";
        auction1.description = "Console for professional players";
        auction1.price = 999.99;
        auction1.seller = new Person();
        auction1.seller.firstName = "Andy";
        auction1.seller.lastName = "Murray";
        auction1.seller.livingAdress = new Adress();
        auction1.seller.livingAdress.city = "London";
        auction1.seller.livingAdress.street = "Main Street";
        auction1.seller.livingAdress.houseNumber = "345";
        auction1.seller.livingAdress.flatNumber = 3;

        Adress adress1 = new Adress();
        adress1.city = "Warsaw";
        adress1.street = "Żurawia";
        adress1.zipCode = "44-556";
        adress1.houseNumber = "453A";
        adress1.flatNumber = 3;

        Person person1 = new Person();
        person1.firstName = "Adam";
        person1.lastName = "Nowak";
        person1.livingAdress = adress1;

        Auction auction2 = new Auction();
        auction2.title = "Samsung S20";
        auction2.description = "Great phone";
        auction2.price = 2499.99;
        auction2.seller = person1;

        System.out.println("Auction1: " + auction1.title + ", description: " + auction1.description + ", price: "
                + auction1.price);
        System.out.println("Seller: " + auction1.seller.firstName + " " + auction1.seller.lastName);

        System.out.println("Auction2: " + auction2.title + ", description: " + auction2.description + ", price: "
                + auction2.price);
        System.out.println("Seller: " + auction2.seller.firstName + " " + auction2.seller.lastName);
    }
}
