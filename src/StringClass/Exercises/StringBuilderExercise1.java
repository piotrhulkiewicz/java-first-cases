package StringClass.Exercises;

import java.util.Scanner;

public class StringBuilderExercise1 {

    public static void main(String[] args) {

        int numberOfWords;
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many words do you want to enter?");
        numberOfWords = scanner.nextInt();
        scanner.nextLine();
        String[] input = new String[numberOfWords];
        for (int i = 0; i < numberOfWords; i++) {
            System.out.println("Enter " + (i + 1) + " word.");
            input[i] = scanner.nextLine();
        }
        StringBuilder output = new StringBuilder();
      for (int i = 0; i < input.length; i++) {
          output.append(input[i].charAt(input[i].length() - 1));
      }
        System.out.println("New world is: "+ output); //Output is a string made with last letters of provided earlier words.

    }
}
