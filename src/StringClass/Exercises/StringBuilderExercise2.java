package StringClass.Exercises;

import java.util.Scanner;

public class StringBuilderExercise2 {

    public static void main(String[] args) {
        System.out.println("Enter phrase to modify");
        Scanner scanner = new Scanner(System.in);
        String origin = scanner.nextLine();
        char first = origin.charAt(0);
        if (Character.isUpperCase(first)) {
            System.out.println(origin.toUpperCase());
        } else if (Character.isLowerCase(first)) {
            System.out.println(origin.toLowerCase());
        } else System.out.println(origin);
    }
}
