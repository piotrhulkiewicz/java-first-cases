package StringClass.Exercises;

import java.util.Random;
import java.util.Scanner;

public class DiceGame {
    public static void main(String[] args) {


        String one = """
                 - - - - - 
                |         |
                |    0    |
                |         |
                 - - - - - """;
        String two = """
                 - - - - - 
                | 0       |
                |         |
                |       0 |
                 - - - - - """;
        String three = """
                 - - - - - 
                | 0       |
                |    0    |
                |       0 |
                 - - - - - """;
        String four = """
                 - - - - - 
                | 0     0 |
                |         |
                | 0     0 |
                 - - - - - """;
        String five = """
                 - - - - - 
                | 0     0 |
                |    0    |
                | 0     0 |
                 - - - - - """;
        String six = """
                 - - - - - 
                | 0     0 |
                | 0     0 |
                | 0     0 |
                 - - - - - """;

        String[] dice = {one, two, three, four, five, six};

        System.out.println("Press Enter to roll the dice");

        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        Random random = new Random();
        int roll = random.nextInt(0, 6);
        System.out.println(dice[roll]);

    }
}