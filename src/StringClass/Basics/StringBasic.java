package StringClass.Basics;

import java.util.Arrays;
import java.util.Scanner;

public class StringBasic {
    public static void main(String[] args) {

        String s1 = "Ania";
        String s2 = "Ania";

        System.out.println(s1==s2);
        System.out.println(s1.equals(s2));

        String s3 = new String("Ania");
        String s4 = new String("Ania");

        System.out.println(s3==s4);
        System.out.println(s3.equals(s4));

//        Scanner scanner = new Scanner(System.in);
//
//        System.out.println("Enter first caption");
//        String s5 = scanner.nextLine();
//
//        System.out.println("Enter second caption");
//        String s6 = scanner.nextLine();
//
//        System.out.println(s5==s6);
//        System.out.println(s5.equals(s6));

        long start = System.nanoTime();
        StringBuilder numbers = new StringBuilder();
        for (int i = 0; i <1000; i++) {
            numbers.append(i).append(" ");
        }
        System.out.println(numbers);
        long time1 = System.nanoTime() - start;

        long start1 = System.nanoTime();
        String numbers2 = "";
        for (int i = 0; i < 1000; i++) {
            numbers2 = numbers2 + i + " ";
        }
        System.out.println(numbers2);
        long time2 = System.nanoTime() - start1;
        System.out.println(time1);
        System.out.println(time2);
        double difference = time2/time1;
        System.out.println(difference);

        String words = "  One two three four five six seven ";
        System.out.println(words.substring(2, 5));
        System.out.println(words);
        System.out.println(words.replaceAll("One", "First"));
        System.out.println(words); //!! Strings are not variable!!
        System.out.println(words.trim());
        System.out.println(words.charAt(2));
        System.out.println(words.toUpperCase());
        System.out.println(words.toLowerCase());
        System.out.println(words.length());
        String[] split = (words.trim().split(" "));
        System.out.println(split.length);
        System.out.println(Arrays.toString(split));
    }
}