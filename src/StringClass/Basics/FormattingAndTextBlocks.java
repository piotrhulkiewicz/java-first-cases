package StringClass.Basics;

public class FormattingAndTextBlocks {
    public static void main(String[] args) {

        //%[index$][flags][weight][.precision][conversion]
        String firstName = "Jan";
        String lastName = "Kowalski";
        //%s
        int age = 23;
        //%d (decimal)
        char gender = 'M';
        //%c
        double height = 1.89;
        //%f (float)

        System.out.println(
                "First Name: " + firstName +
                ", Last Name: " + lastName +
                ", Age: " + age +
                ", Gender: "  + gender +
                ", Height: " + height
        );
        System.out.printf("First Name: %s, Last Name: %s, Age: %d, Gender: %c, Height: %f%n",//%n
                firstName, lastName, age, gender, height);

        System.out.printf("First Name: %2$s, Last Name: %3$s, Age: %4$d, Gender: %5$c, Height: %1$f%n",//%n
                height, firstName, lastName, age, gender);


        String newString = String.format("First Name: %s, Last Name: %s, Age: %d, Gender: %c, Height: %f%n",
                firstName, lastName, age, gender, height);
        System.out.println(newString);

        String format = "First Name: %s, Last Name: %s, Age: %d, Gender: %c, Height: %f%n";
        String newString2 = format.formatted(firstName, lastName, age, gender, height);

        //precision

        double number = 123.4567890;
        System.out.printf("%.2f%n", number);

        //weight
        System.out.printf("%10.2f%n", number);

        String name = "Jan";
        System.out.printf("%10s%n", name);

        double positiveNumber = 123.456789;
        System.out.printf("%+.2f%n", positiveNumber);

        double negativeNumber = -1.234;
        System.out.printf("%+010.2f%n", negativeNumber);
        System.out.printf("%(010.2f%n", negativeNumber); // () instead of '-'

        int bigNumber = 123456789;
        System.out.printf("%,d%n", bigNumber);

        String name1 = "Anna";
        String name2 = "Piotr";
        int age1 = 30;
        int age2 = 35;
        System.out.printf("%-20s%s%n", "Name", "Age");
        System.out.printf("%-20s%d%n", name1, age1);
        System.out.printf("%-20s%d%n", name2, age2);


        String serchTitle = "Lord of the rings";
        String sqlQuerry =
                "SELECT\n" +
                        "    id, title, description\n" +
                "FROM\n" +
                        "    books\n" +
                "WHERE\n" +
                        "    title = '%s'\n";
        System.out.println(sqlQuerry.formatted(serchTitle));

        String sqlQuerry2 = """
                SELECT
                    id, title, description
                FROM
                    books
                WHERE
                    title = "%s"
                """;
        String sgl2 = sqlQuerry2.formatted(serchTitle);
        System.out.println(sgl2);
        System.out.println(sgl2.indent(4)); // number of extra spaces

    }
}
