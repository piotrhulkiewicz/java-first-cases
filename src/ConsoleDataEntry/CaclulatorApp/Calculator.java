package ConsoleDataEntry.CaclulatorApp;

import java.util.Locale;
import java.util.Scanner;

public class Calculator {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        scanner.useLocale(Locale.US);

        System.out.println("Enter a");
        double a = scanner.nextDouble();
        System.out.println("Enter b");
        double b = scanner.nextDouble();
        scanner.nextLine();
        System.out.println("Select an action: ");
        System.out.println("To add enter '+' ");
        System.out.println("To subtract enter '-' ");
        System.out.println("To multiply enter '*' ");
        System.out.println("To divide enter '/' ");
        String action = scanner.nextLine();

        scanner.close();

        Calculate calculate = new Calculate();
        calculate.calculateAndPrint(a, b, action);

    }
}
