//This program is an implementation of algorithm used in
// https://app.javastart.pl/kurs/java/java-podstawy-algorytmika/cwiczenie/java-algorytmika-cwiczenie-2

package AlgorithmicsExercises;

import java.util.Scanner;

public class MultiplyNumbersEvenOdd {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int multi = getMulti(scanner);
        printPositiveOrNegative(multi);
    }

    private static int getMulti(Scanner scanner) {
        int multi = 1;
        int next = 0;

        for (int index = 0; index < 5; index++) {
            System.out.println("Podaj " + (index + 1) + " liczbę.");
            next = scanner.nextInt();
            multi *= next;
        }
        return multi;
    }

    private static void printPositiveOrNegative (int multi){
            if (multi >= 0) System.out.println("Product of entered numbers: " + multi + " positive ");
            else System.out.println("Product of entered numbers: " + multi + " is negative");
        }
    }
