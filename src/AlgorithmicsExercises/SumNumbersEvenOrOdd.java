//This program is an implementation of algorithm used in
// https://app.javastart.pl/kurs/java/java-podstawy-algorytmika/cwiczenie/java-algorytmika-cwiczenie-1

package AlgorithmicsExercises;

import java.util.Scanner;

public class SumNumbersEvenOrOdd {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int sum = getSum(scanner);
        printOddOrEven(sum);
        scanner.close();
    }

    private static void printOddOrEven(int sum) {
        if (sum % 2 == 0) System.out.println("Sum of entered numbers: " + sum + " is even");
        else System.out.println("Sum of entered numbers: " + sum + " is odd");
    }

    private static int getSum(Scanner scanner) {
        int sum = 0;
        int number;
        System.out.println("Enter number to sum, number =< 100 ends program in print result");
        while ((number = scanner.nextInt()) <= 100) {
            System.out.println("Enter number to sum, number =< 100 ends program in print result");
            sum = sum + number;
        }
        return sum;
    }
}
