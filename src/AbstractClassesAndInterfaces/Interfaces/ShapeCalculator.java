package AbstractClassesAndInterfaces.Interfaces;


public class ShapeCalculator{


    public static void main(String[] args) {

        ShapeController shapeController = new ShapeController();

        int option = shapeController.getOption();
        Shape shape = shapeController.createNewShape(option);
        shape.calculatePerimeter();
        shape.calculateArea();
        System.out.println(shape.getData());
    }
}

