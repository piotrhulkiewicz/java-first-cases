package AbstractClassesAndInterfaces.Interfaces;

public class ShapeController {

    DataReader dataReader = new DataReader();

    public int getOption() {
        int option = 0;
        try {
            option = dataReader.getOption();
        } catch (IllegalStateException e) {
            System.err.println(e.getMessage());
        }
        return option;
    }

    public Shape createNewShape(int option) {
        Shape shape;
        switch (option) {
            case DataReader.EXIT -> exit();
            case DataReader.TRIANGLE -> {
                shape = createTriangle();
                return shape;
            }
            case DataReader.RECTANGLE -> {
                shape = createRectangle();
                return shape;
            }
            case DataReader.CIRCLE -> {
                shape= createCircle();
                return shape;
            }
        } throw new IllegalStateException("Illegal option");
    }

    private Circle createCircle() {
        System.out.println("Enter circle radius ");
        double radius = dataReader.getDouble();
        return new Circle(radius);
    }

    private Rectangle createRectangle() {
        System.out.println("Enter a and b");
        System.out.println("Enter a");
        double a = dataReader.getDouble();
        System.out.println("Enter b");
        double b = dataReader.getDouble();
        return new Rectangle(a, b);
    }

    private Triangle createTriangle() {
        System.out.println("Enter a,b,c");
        System.out.println("Enter a");
        double a = dataReader.getDouble();
        System.out.println("Enter b");
        double b = dataReader.getDouble();
        System.out.println("Enter c");
        double c = dataReader.getDouble();
        return new Triangle(a, b, c);
    }

    private void exit() {
        dataReader.close();
    }
}
