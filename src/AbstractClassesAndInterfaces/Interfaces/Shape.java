package AbstractClassesAndInterfaces.Interfaces;

interface Shape {

    double PI = 3.14;

    double calculateArea();

    double calculatePerimeter();

    default String getData  () {
        return "Area is " + calculateArea() + ", perimeter is " + calculatePerimeter();
    }
}
