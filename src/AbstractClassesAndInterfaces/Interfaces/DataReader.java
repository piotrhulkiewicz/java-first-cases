package AbstractClassesAndInterfaces.Interfaces;

import java.util.InputMismatchException;
import java.util.Scanner;

public class DataReader {

    static final int EXIT = 0;
    final static int TRIANGLE = 1;
    final static int RECTANGLE = 2;
    final static int CIRCLE = 3;

    private Scanner scanner = new Scanner(System.in);


    public int getOption () {
        int option = -1;
        System.out.println("Enter the shape you want to calculate" );
        System.out.println("Exit program - " + EXIT);
        System.out.println("Triangle - " + TRIANGLE );
        System.out.println("Rectangle - " + RECTANGLE );
        System.out.println("Circle - " + CIRCLE );
        try {
            option = scanner.nextInt();
            if (option < 0 || option > 3) {
                throw new IllegalStateException("Invalid option. Try again ");
            }
        } catch (InputMismatchException e) {
            System.err.println("You can only enter number");
        }
        scanner.nextLine();
        return option;
    }

    public void close() {
        scanner.close();
    }

    public double getDouble() {
        double a = scanner.nextDouble();
        if (a > 0) {
            scanner.nextLine();
            return a;

        } else throw new IllegalStateException("Enter only positive number");
    }
}
