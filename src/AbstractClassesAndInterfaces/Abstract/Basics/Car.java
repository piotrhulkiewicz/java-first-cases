package AbstractClassesAndInterfaces.Abstract.Basics;

public class Car extends Vehicle{

    public void speedUp () {
        setSpeed(getSpeed() + 5);
    }

    @Override
    public String toString() {
        return "Car speed is: " + super.getSpeed();
    }
}
