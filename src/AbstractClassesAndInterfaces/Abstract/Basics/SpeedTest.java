package AbstractClassesAndInterfaces.Abstract.Basics;

public class SpeedTest {

    public static void main(String[] args) {

        Vehicle car = new Car();
        Vehicle plane = new Plane();
  //      Vehicle vehicle = new Vehicle(); you cant make an object of abstract class

        ((Car) car).speedUp(); // change type or make method in class above
        plane.speedUp();

        System.out.println(plane);
        System.out.println(car);
    }
}
