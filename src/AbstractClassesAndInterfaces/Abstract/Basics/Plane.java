package AbstractClassesAndInterfaces.Abstract.Basics;

public class Plane extends Vehicle{

    public void speedUp () {
        setSpeed(getSpeed() + 100);
    }

    @Override
    public String toString() {
        return "Plain speed is: " + super.getSpeed();
    }
}
