package AbstractClassesAndInterfaces.Abstract.Exercise;

public abstract class Employee {

    private String firstName;
    private String lastName;


    public Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    public abstract double calculateMonthSalary();
    public abstract double calculateAnnualSalary();

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("Employee " + firstName + " " + lastName);
        stringBuilder.append(", month salary: " + calculateMonthSalary());
        stringBuilder.append(", annual salary: " + calculateAnnualSalary());
     return stringBuilder.toString();
    }
}
