package AbstractClassesAndInterfaces.Abstract.Exercise;

public class Firm {

    public static void main(String[] args) {

        Employee employee1 = new RegularEmployee("Merry", "Pippin", 3400);
        Employee employee2 = new UnregularEmployee("Peregrin", "Tuk", 125, 29);

        Employee[] employees = new Employee[2];
        employees[0] = employee1;
        employees[1] = employee2;
        double sumOfMonthSalary = 0;
        double sumOfAnnualSalary = 0;

        for (Employee employee : employees) {
            sumOfMonthSalary += employee.calculateMonthSalary();
            sumOfAnnualSalary += employee.calculateAnnualSalary();
        }

        for (Employee employee : employees) {
            System.out.println(employee);
        }
        System.out.println("Sum od month salary: " + sumOfMonthSalary);
        System.out.println("Sum od annual salary: " + sumOfAnnualSalary);
    }
}
