package AbstractClassesAndInterfaces.Abstract.Exercise;

public class RegularEmployee extends Employee{

    private static final double ANNUAL_BONUS = 0.05;
    private double salary;
    public RegularEmployee(String firstName, String lastName, double salary) {
        super(firstName, lastName);
        this.salary = salary;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public double calculateMonthSalary() {
        return getSalary();
    }

    @Override
    public double calculateAnnualSalary() {
        double annualSalary = 0;
        annualSalary = getSalary() * 12;
        double annualBonus = annualSalary * ANNUAL_BONUS;
        annualSalary = annualSalary+ annualBonus;
        return annualSalary;
    }
}
