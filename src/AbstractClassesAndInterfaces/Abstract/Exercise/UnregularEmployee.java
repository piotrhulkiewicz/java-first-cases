package AbstractClassesAndInterfaces.Abstract.Exercise;

public class UnregularEmployee extends Employee{

    private int numberOfHours;
    private double hourlyRate;

    public UnregularEmployee(String firstName, String lastName, int numberOfHours, double hourlyRate) {
        super(firstName, lastName);
        this.numberOfHours = numberOfHours;
        this.hourlyRate = hourlyRate;
    }

    public int getNumberOfHours() {
        return numberOfHours;
    }

    public void setNumberOfHours(int numberOfHours) {
        this.numberOfHours = numberOfHours;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    @Override
    public double calculateMonthSalary() {
        return numberOfHours * hourlyRate;
    }

    @Override
    public double calculateAnnualSalary() {
        return calculateMonthSalary() * 12;
    }
}
