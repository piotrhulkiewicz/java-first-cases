package AbstractClassesAndInterfaces.Other;

public interface Boat {
    default void printName() {
        System.out.print("Boat");
    }
}
