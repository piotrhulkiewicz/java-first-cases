package AbstractClassesAndInterfaces.Other;

public class Amphibia implements Car, Boat{
    @Override
    public void printName() {

        System.out.print("Amphibia is a little bit ");
        Car.super.printName();
        System.out.print(" and a little bit ");
        Boat.super.printName();
    }
}
