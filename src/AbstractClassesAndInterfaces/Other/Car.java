package AbstractClassesAndInterfaces.Other;

public interface Car {

    default void printName() {
        System.out.print("Car");
    }
}
