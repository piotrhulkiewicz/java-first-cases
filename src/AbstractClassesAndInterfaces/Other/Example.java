package AbstractClassesAndInterfaces.Other;

public interface Example {

    default void complicatedMethod() {
        read();
        calculate();
        save();
    }

    private void save() {};

    private void calculate() {};

    private void read() {};
}
